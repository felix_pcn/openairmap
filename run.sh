#!/usr/bin/env bash

echo "Warning: Local run only for development purpose"
echo "For regular usage please install using setup"
python ./openairmap/app.py
