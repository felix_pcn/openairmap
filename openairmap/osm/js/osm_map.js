var map;
var ajaxRequest;
var plotlist;
var plotlayers=[];
var websocket = new WebSocket("ws://127.0.0.1:8765/");
var data_map = [];
var markers = [];
var selected_marker = [];
var default_zoom = 10;

var marker_select_icon = {
  default: L.icon({iconUrl: 'js/images/marker_select_default.svg', iconSize: [18, 28]}),
  warning: L.icon({iconUrl: 'js/images/marker_select_warning.svg', iconSize: [18, 28]}),
  error: L.icon({iconUrl: 'js/images/marker_select_error.svg', iconSize: [18, 28]}),
};

function initmap() {
  map = new L.Map('mapid', {zoomAnimation: false, attributionControl: false});
  var mbxUrl='https://{s}.tiles.mapbox.com/v4/mapbox.streets/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiZ25vbWUtbWFwcyIsImEiOiJjaXF3a3lwbXkwMDJwaTBubmZlaGk4cDZ6In0.8aukTfgjzeqATA8eNItPJA'
  var mbx = new L.TileLayer(mbxUrl, {minZoom: 8, maxZoom: 20});
  mbx.on('tileerror ',  function (event) {
    console.log('Error')
  });
  map.addLayer(mbx);
}

function get_title(data, format) {
  if (format === 'html')
    return data.date+"<br /><b>Pm2.5:"+data.pm25+"<br />Pm10:"+data.pm10+"</b>"
  return data.date+" Pm2.5 "+data.pm25+" - Pm10 "+data.pm10
}

// websocket.onclose = function (event) {
//   console.log('Websocket closed');
// }

websocket.onopen = function (event) {
  setInterval(function() {websocket.send('ping')}, 100);
}

websocket.onmessage = function (event) {
  // console.log('New message: ' + event.data);
  if (event.data === 'pong') return;
  let data = JSON.parse(event.data);
  // console.log(JSON.stringify(data));
  if (data.hasOwnProperty('zoom')) {
    default_zoom = data.zoom;
  }
  if (data.hasOwnProperty('map')) {
    let data_map = data.map;
    for (let marker of markers) {
      map.removeLayer(marker);
    }
    markers = [];
    for (let data of data_map) {
      let marker = L.marker([data.latitude, data.longitude],
                            {icon: marker_icon[data.tag], opacity: .85, title: get_title(data)})
      marker.addTo(map);
      markers.push(marker);
    }
    if (data.map.length > 1)
      map.setView(new L.LatLng(data.map[0].latitude, data.map[0].longitude), default_zoom);
  }
  if (data.hasOwnProperty('selected')) {
    map.removeLayer(selected_marker);
    data = data.selected;
    selected_marker = L.marker([data.latitude, data.longitude],
                               {icon: marker_select_icon[data.tag], zIndexOffset: 100});
    selected_marker.addTo(map);
    selected_marker.bindPopup(get_title(data,'html'), {className: data.tag}).openPopup();
    map.setView(new L.LatLng(data.latitude, data.longitude), default_zoom);
  }
}

initmap();
