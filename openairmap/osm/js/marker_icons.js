var marker_icon = {
  default: L.icon({iconUrl: 'js/images/marker_default.svg', iconSize: [10, 10]}),
  warning: L.icon({iconUrl: 'js/images/marker_warning.svg', iconSize: [10, 10]}),
  danger: L.icon({iconUrl: 'js/images/marker_danger.svg', iconSize: [10, 10]}),
};

var marker_select_icon = {
  default: L.icon({iconUrl: 'js/images/marker_select_default.svg', iconSize: [18, 28]}),
  warning: L.icon({iconUrl: 'js/images/marker_select_warning.svg', iconSize: [18, 28]}),
  danger: L.icon({iconUrl: 'js/images/marker_select_danger.svg', iconSize: [18, 28]}),
};

// var marker_default_icon = L.icon({
//   iconUrl: 'js/images/marker_default.svg',
//   iconSize: [10, 10],
// });
//
// var marker_warning_icon = L.icon({
//   iconUrl: 'js/images/marker_warning.svg',
//   iconSize: [10, 10],
// });
//
// var marker_danger_icon = L.icon({
//   iconUrl: 'js/images/marker_warning.svg',
//   iconSize: [10, 10],
// });
//
// var marker_select_default_icon = L.icon({
//   iconUrl: 'js/images/marker_select_default.svg',
//   iconSize: [18, 28],
// });
//
// var marker_select_warning_icon = L.icon({
//   iconUrl: 'js/images/marker_select_warning.svg',
//   iconSize: [18, 28],
// });
//
// var marker_select_danger_icon = L.icon({
//   iconUrl: 'js/images/marker_select_warning.svg',
//   iconSize: [18, 28],
// });
