import asyncio
import logging
import threading
import websockets

class WsServer(threading.Thread):
    def __init__(self, loop, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.loop = loop
        self.start_server = None
        self.websocket = None
        self.shutdown_request = False

    async def pong(self, websocket, unused_path):
        while not self.shutdown_request:
            self.websocket = websocket
            await websocket.recv()
            await self.websocket.send('pong')

    async def _send(self, message):
        while not self.websocket:
            logging.warning('Waiting for client')
            await asyncio.sleep(1)
        if logging.root.level == logging.DEBUG:
            log_msg = f'{message[:80]} (...)' if len(message) > 80 else message
            logging.debug('Sending message: %s', log_msg)
        await self.websocket.send(message)

    def send(self, message):
        self.loop.create_task(self._send(message))

    def run(self):
        self.start_server = websockets.serve(self.pong, '127.0.0.1', 8765,
                                             loop=self.loop)
        self.loop.run_until_complete(self.start_server)
        self.loop.run_forever()

    def shutdown(self):
        logging.info('Ws server shutdown request')
        self.shutdown_request = True
        self.loop.stop()
