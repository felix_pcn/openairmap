import os
import locale
import gettext
import xdg.BaseDirectory as bd
from openairmap.config import PACKAGE_NAME, LOCALE_DIR


# bind textdomain for GTK Builder
locale.bindtextdomain(PACKAGE_NAME, LOCALE_DIR)

# gettext.install(PACKAGE_NAME, LOCALE_DIR)
# lang1 = gettext.translation(PACKAGE_NAME, LOCALE_DIR, languages=['fr'])
# lang1.install()

# add gettext shortcut "_" for string translations
_ = gettext.translation(
    domain=PACKAGE_NAME, localedir=LOCALE_DIR, fallback=True).gettext

def get_realpath():
    try:
        return os.path.dirname(os.path.realpath(__file__))
    except NameError:
        return None

def get_paths():
    return ['./data'] + list(bd.load_data_paths(PACKAGE_NAME))

def get_file(filename):
    """
    Return path to @filename if it exists
    anywhere in the data paths, else return None
    """
    for path in get_paths():
        file_path = os.path.join(path, filename)
        if os.path.exists(file_path):
            return file_path
    return None
