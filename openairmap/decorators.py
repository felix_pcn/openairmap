import logging
import gi
# pylint: disable=C0413
gi.require_version('GLib', '2.0')
from gi.repository import GLib

def need_device(func):
    """ Decorator disabling function when no serial object is found """
    def wrapper(self, *args, **kwargs):
        if not self.serial:
            return None
        return func(self, *args, **kwargs)
    return wrapper

def log_exception(func):
    """ Decorator for exception logging """
    def wrapper(self, *args, **kwargs):
        try:
            return func(self, *args, **kwargs)
        except Exception:
            logging.warning('', exc_info=True)
            return None
    return wrapper

def check_ready(func):
    """ Decorator disabling signals until UI is ready, checking self.ready
        property state
    """
    def wrapper(self, *args, **kwargs):
        if not self.ready:
            return None
        return func(self, *args, **kwargs)
    return wrapper

def thread_safe(func):
    """ Decorator disabling signals until UI is ready, checking self.ready
        property state
    """
    def wrapper(self, *args, **kwargs):
        if not self.ready:
            return None
        return GLib.idle_add(lambda: func(self, *args, **kwargs))
    return wrapper
