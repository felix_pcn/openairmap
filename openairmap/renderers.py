import logging

def render_datetime(unused_col, cell, model, index, unused_userdata):
    try:
        value = model.get(index, 0)[0]
    except TypeError:
        return
    cell.set_property('text', value.human_date)

def render_float(col, cell, model, _iter, unused_userdata):
    tview = col.get_tree_view()
    for idx, tvcol in enumerate(tview.get_columns()):
        if col != tvcol:
            continue
        try:
            value = model.get(_iter, idx)[0]
        except TypeError:
            break
        cell.set_property('text', str(round(value, 2)))
        break

def render_status(unused_col, cell, model, index, unused_userdata):
    data = model.get(index, 0)[0]
    color = 'green'
    if data.tag == 'danger':
        color = 'red'
    if data.tag == 'warning':
        color = 'orange'
    cell.set_property('background', color)
