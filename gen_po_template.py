#!/bin/env python

# extracts strings from *.py and .ui files and
# generates a gettext .pot template.
# update every existing po file with new strings

import os
from glob import glob

from openairmap.config import PACKAGE_NAME

GLADE_DIR = './data'
PYTHON_DIR = f'./{PACKAGE_NAME}'
POT_FOLDER = './po'
POT_FILE = os.path.join(POT_FOLDER, f'{PACKAGE_NAME}.pot')

def list_files(path, extension):
    file_list = filter(os.path.isfile, glob(os.path.join(path, '*')))
    return filter(lambda f: os.path.splitext(f)[1].lower() == f'.{extension}',
                  file_list)

if not os.path.exists(POT_FOLDER):
    os.makedirs(POT_FOLDER)
elif os.path.exists(POT_FILE):
    os.remove(POT_FILE)

# generate string headers of all glade files
for glade_file in list_files(GLADE_DIR, 'ui'):
    print(f'Generating string headers from glade file {glade_file}')
    os.system(f'intltool-extract --type=gettext/glade {glade_file}')

# write template files
PY_FILES = str()
for py_file in list_files(PYTHON_DIR, 'py'):
    print(f'Adding python source file {py_file}')
    PY_FILES += f'{py_file} '
if PY_FILES:
    os.system(f'xgettext {PY_FILES} {GLADE_DIR}/*.h --keyword=_ --keyword=N_ '
              + f'--from-code=UTF-8 --output={POT_FILE}')

# update every existing po file
for po_file in list_files(POT_FOLDER, 'po'):
    print(f'Updating po file {po_file}')
    os.system(f'msgmerge --update --backup=none {po_file} {POT_FILE}')

# clean up
for h_file in list_files(GLADE_DIR, 'h'):
    print(f'Removing generated header file {h_file}')
    os.remove(h_file)

        #msgmerge --update --backup=none fr.po openairmap.pot
