#!/usr/bin/env python3
import os
import glob
import shutil
# from argparse import ArgumentParser
from distutils.core import setup
from distutils.cmd import Command
from distutils.command.install_data import install_data
from distutils.command.build import build

from openairmap.config import PACKAGE_NAME, APP_VERSION

BUILD_DIR = 'build'
BUILD_LOCALE_DIR = os.path.join(BUILD_DIR, 'locale')

def gen_locales():
    for po_file in glob.glob('./po/*.po'):
        lang = os.path.splitext(os.path.basename(po_file))[0]
        dest_path = os.path.join(BUILD_LOCALE_DIR, lang, 'LC_MESSAGES')
        if not os.path.exists(dest_path):
            os.makedirs(dest_path)
        mo_file = os.path.join(dest_path, f'{PACKAGE_NAME}.mo')
        print(f'Creating {mo_file}')
        os.system(f'msgfmt -o {mo_file} {po_file}')

class BuildData(build):
    def run(self):
        # Create translation mo files
        gen_locales()
        build.run(self)
        # Add js files
        osm_path = os.path.join(BUILD_DIR, 'lib', 'openairmap', 'osm')
        if os.path.exists(osm_path):
            shutil.rmtree(osm_path)
        shutil.copytree(f'{PACKAGE_NAME}/osm', osm_path)


class InstallData(install_data):
    def add_data_files(self, data_files, walk_path, root_path, dst_path):
        for data_file in data_files:
            src_path = os.path.join(root_path, data_file)
            dst_path = os.path.join(
                dst_path, os.path.dirname(src_path[len(walk_path)+1:]))
            self.data_files.append((dst_path, [src_path]))

    def run(self):
        # Install translation po files
        for root_path, _, files in os.walk(BUILD_LOCALE_DIR):
            self.add_data_files(files, BUILD_LOCALE_DIR, root_path,
                                'share/locale')
        # Install  icons
        for root_path, _, files in os.walk('data/icons'):
            self.add_data_files(files, 'data/icons', root_path, 'share/icons')
        os.system('gtk-update-icon-cache /usr/share/icons/hicolor')
        install_data.run(self)


class Uninstall(Command):
    def run(self):
        # TODO
        pass


setup(
    name=PACKAGE_NAME,
    version=APP_VERSION,
    description='FOSS air quality sensor mapping interface',
    author='Razer',
    author_email='razerraz@free.fr',
    url='https://framagit.org/razer/openairmap',
    license='GNU GPLv3',
    packages=[PACKAGE_NAME],
    scripts=[os.path.join('bin', PACKAGE_NAME)],
    data_files=[
        (os.path.join('share', PACKAGE_NAME), glob.glob('data/*.ui')),
        (os.path.join('share', PACKAGE_NAME), glob.glob('data/*.css')),
        (os.path.join('share', PACKAGE_NAME), [f'data/{PACKAGE_NAME}.png']),
        ('share/appdata', [f'data/{PACKAGE_NAME}.appdata.xml']),
        ('share/applications', [f'data/{PACKAGE_NAME}.desktop']),
        ],
    cmdclass={
        'build': BuildData,
        'install_data': InstallData,
        'uninstall': Uninstall}
    )
