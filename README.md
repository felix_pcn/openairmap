## Open Air Map
![Logo](https://framagit.org/razer/openairmap/raw/master/artwork/oam-icon.svg)

![Screenshot](https://framagit.org/razer/openairmap/raw/master/artwork/oam-shot-main.png)

## In developpement - Use at your own risks

OpenAirMap is a GTK application providing a map with pollution data (actually pm2.5 and pm10 particules) coming from an ESP16/32 mobile device with NovaPm (or other dust sensor) and a GPS.

The device firmware can be built using source code from the device folder and the Arduino IDE or PlatformIO with Arduino support.

For Arduino software usage, just rename main.cpp -> main.ino.

The const.h file should be edited with pin definition.

Currently, only Expressif hardware is supported : ESP8266 or ESP32.
Electrical wiring specs will be added soon as device own making support.
