#include <screen.h>

void config_page(SSD1306 * display, Config cfg, State state) {
  display->clear();
  String sleep_mode = cfg.sleep_mode ? "Screen" : "Off";
  if (cfg.sleep_mode == 2) sleep_mode += "+GPS";
  if (state.menu_index >= 30) display->fillRect(0,21+(11*(state.menu_index-30)),128,10);
  display->setFont(ArialMT_Plain_16);
  display->setTextAlignment(TEXT_ALIGN_CENTER);
  String title = (state.menu_index >= 30) ? "Config menu" : "Config update";
  display->drawString(60, 0, title);
  display->setFont(ArialMT_Plain_10);
  display->setTextAlignment(TEXT_ALIGN_LEFT);
  if (state.menu_index == 30) display->setColor(BLACK);
  display->drawString(0, 20, "Sleep mode:");
  display->setTextAlignment(TEXT_ALIGN_RIGHT);
  display->drawString(128, 20, sleep_mode);
  display->setColor(WHITE);
  if (state.menu_index == 31) display->setColor(BLACK);
  display->setTextAlignment(TEXT_ALIGN_LEFT);
  display->drawString(0, 31, "Data freq (min):");
  display->setTextAlignment(TEXT_ALIGN_RIGHT);
  display->drawString(128, 31, String(cfg.data_freq));
  display->setColor(WHITE);
  if (state.menu_index == 32) display->setColor(BLACK);
  display->setTextAlignment(TEXT_ALIGN_LEFT);
  display->drawString(0, 42, "GPS timeout (min):");
  display->setTextAlignment(TEXT_ALIGN_RIGHT);
  display->drawString(128, 42, String(cfg.gps_timeout));
  display->setColor(WHITE);
  if (state.menu_index >= 30) {
    display->setTextAlignment(TEXT_ALIGN_LEFT);
    if (state.menu_index == 33) display->setColor(BLACK);
    display->drawString(0, 53, state.config_updated ? "Save" : "Back");
    display->setColor(WHITE);
  }
  display->display();
}


void fetch_page(SSD1306 * display, Data data, Config cfg, SavedState saved_state,
                State state) {
  display->clear();
  display->setTextAlignment(TEXT_ALIGN_LEFT);
  display->drawString(0, 0, format_date(day(), month(), year()) + " "
                           + format_time(hour(), minute(), second()));
  display->setTextAlignment(TEXT_ALIGN_RIGHT);
  display->drawString(128, 0, String(saved_state.data_count));
  display->setTextAlignment(TEXT_ALIGN_CENTER);
  String counter = String(seconds()-state.fetch_start);
  if (data.p10) {
    display->setFont(ArialMT_Plain_16);
    display->drawString(60, 18, "PM2.5: " + String(data.p25));
    display->drawString(60, 32, "PM10: " + String(data.p10));
    display->setFont(ArialMT_Plain_10);
  } else {
    display->drawString(60, 26, "Waiting for PM levels:" + counter
                               + "/" + String(cfg.nova_wakeup_delay));
  }
  if (data.lat>0) {
    display->drawString(60, 54, "Lat:" + String(data.lat,4)
                               + " Lng:" + String(data.lng,4));
  } else if (state.on_fetch) {
    display->drawString(60, 54, "Waiting for GPS:" + counter
                               + "/" + String(cfg.gps_timeout*60));
  }
  display->display();
}

void menu_page(SSD1306 * display, Config cfg, State state) {
  display->clear();
  display->setColor(WHITE);
  display->setFont(ArialMT_Plain_16);
  display->setTextAlignment(TEXT_ALIGN_CENTER);
  display->drawString(64, 0, "Main menu");
  display->fillRect(8,21+(11*state.menu_index),120,10);
  display->setFont(ArialMT_Plain_10);
  if (state.menu_index == 0) display->setColor(BLACK);
  display->drawString(64, 20, "Fetch data");
  display->setColor(WHITE);
  if (state.menu_index == 1) display->setColor(BLACK);
  display->drawString(64, 31, "Enable Bluetooth");
  display->setColor(WHITE);
  if (state.menu_index == 2) display->setColor(BLACK);
  display->drawString(64, 42, "Config");
  display->setColor(WHITE);
  if (state.menu_index == 3) display->setColor(BLACK);
  display->drawString(64, 53, "Back");
  display->setColor(WHITE);
  display->display();
}

void eeprom_error_page(SSD1306 * display) {
  display->setFont(ArialMT_Plain_16);
  display->setTextAlignment(TEXT_ALIGN_CENTER);
  display->drawString(60, 30, "Storage Error !");
  display->setFont(ArialMT_Plain_10);
  display->setTextAlignment(TEXT_ALIGN_LEFT);
  display->display();
  delay(1000000);
}

uint8_t _set_display_on(SSD1306 * display,  State * state) {
  if (!state->display_onpower) {
    state->display_onpower = true;
    display->displayOn();
  }
  return true;
}

uint8_t set_display_power(SSD1306 * display, Config cfg, State * state) {
  if (state->config_updated | state->on_fetch | (state->menu_index != -1))
    return _set_display_on(display, state);
  if (millis() - state->last_data_fetch < LCD_DISPLAY_TIMEOUT*1000)
    return _set_display_on(display, state);
  if (state->display_onpower & (cfg.sleep_mode != 0)) {
    state->display_onpower = false;
    display->displayOff();
  }
  return (cfg.sleep_mode == 0);
}

void init_display(SSD1306 * display, State * state) {
  display->init();
  display->flipScreenVertically();
  display->clear();
  state->display_onpower = true;
}
