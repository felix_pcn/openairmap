#include <utils.h>

String format_time(int hour, int mins, int sec) {
  String time_str;
  if (hour < 10) time_str = "0";
  time_str += String(hour) + ":";
  if (mins < 10) time_str += "0";
  time_str += String(mins) + ":";;
  if (sec < 10) time_str += "0";
  time_str += String(sec);
  return time_str;
}

String format_date(int day, int mon, int year) {
  String date_str;
  if (day < 10) date_str = "0";
  date_str += String(day) + "/";
  if (mon < 10) date_str += "0";
  date_str += String(mon) + "/";
  date_str += String(year);
  return date_str;
}

uint16_t get_next_map_value(const uint8_t * map, uint16_t value) {
  uint8_t idx;
  for (idx=0; idx<sizeof(map); idx++) {
    if (map[idx] == value) break;
  }
  idx = (idx >= sizeof(map)) ? 0 : idx+1;
  return map[idx];
}

uint16_t seconds () {
  return (millis()/1000);
}
