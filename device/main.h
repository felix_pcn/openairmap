#ifndef _Main_H_
#define _Main_H_

#include "Time.h"
#include "SDS011.h"
#include "SSD1306.h"
#include "TinyGPS++.h"
#include "EEPROM.h"
#include "const.h"
#include "struct.h"
#include "utils.h"
#include "screen.h"

void refresh_screen();
void update_time();
void blink_led();
void fetch_new_data();
void read_serial();
void read_switchs();
void read_gps();
void read_novapm();
void set_gps_power(uint8_t state);

#endif /* _Main_H_ */
