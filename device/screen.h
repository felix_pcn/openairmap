#ifndef _Screen_H_
#define _Screen_H_

#include "Arduino.h"
#include "Time.h"
#include "SSD1306.h"
#include "const.h"
#include "struct.h"
#include "utils.h"

void init_display(SSD1306 * display, State * state);
void fetch_page(SSD1306 * display, Data data, Config cfg, SavedState saved_date,
                State state);
void menu_page(SSD1306 * display, Config cfg, State state);
void config_page(SSD1306 * display, Config cfg, State state);
uint8_t set_display_power(SSD1306 * display, Config cfg, State * state);
void eeprom_error_page(SSD1306 * display);
#endif /* _Screen_H_ */
